#!make

BUILD_CACHE_PREFIX="ideaplexus/"
PROXY_CACHE_PREFIX=""
MYSQL_VERSION="8"

.PHONY: lint
lint:
	docker run --rm -i -v ${PWD}/.hadolint.yml:/bin/hadolint.yml -e XDG_CONFIG_HOME=/bin hadolint/hadolint:latest-alpine < Dockerfile

.PHONY: build
build:
	docker build --file Dockerfile --tag $(BUILD_CACHE_PREFIX)mysql --build-arg "PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX)" --build-arg "MYSQL_VERSION=$(MYSQL_VERSION)"  .

.PHONY: all
all: lint build